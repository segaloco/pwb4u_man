.TH SDB 1 "not on PDP-11"
.SH NAME
sdb \- symbolic debugger
.SH SYNOPSIS
.B sdb
[ objfil [ corfil [ directory ] ] ]
.SH DESCRIPTION
.I Sdb\^
is a symbolic debugger which can be used with C and F77 programs.
It may be used to examine their files and to provide
a controlled environment for their execution.
.PP
.I Objfil\^
is an executable program file
which has been compiled with the
.B \-g
(debug) option.
The default for
.I objfil\^
is
.BR a.out .
.I Corfil\^
is assumed to be a core image file produced after
executing
.IR objfil ;
the default for
.I corfil\^
is
.BR core .
The core file need not be present.
Source file names in
.I objfil\^
are interpreted relative to
.I directory.
.PP
It is useful to know that at any time there is a
.I "current line\^"
and
.IR "current file" .
If
.I corfil\^
exists then they are initially set to the line and file
containing the source statement at which the process terminated or stopped.
Otherwise, they are set to the first line in
.IR main ().
The current line and file may be changed with the source file
examination commands.
.PP
Names of variables are written just as they are in C or F77.
Variables local to a procedure may be accessed using the form
.IB procedure : variable\fR.
If no procedure name is given, the procedure containing the
current line is used by default.
It is also possible to refer to structure members as
.IB variable . member\fR,
pointers to structure members as
.IB variable \(mi> member
and array elements
as
.IB variable [ number ]\fR.
Combinations of these forms may also be used.
.PP
It is also possible to specify a variable by its address.
All forms of integer constants which are valid in C may be used, so that
addresses may be input in decimal, octal or hexadecimal.
.PP
Line numbers in the source program are referred to as
.IB file-name : number
or
.IB procedure : number\fR.
In either case the number is relative to the beginning of the file.
If no procedure or file name is given,
the current file is used by default.
If no number is given,
the first line of the named procedure or file is used.
.PP
While a process is running under
.IR sdb
all addresses refer to the executing program;
otherwise they refer to
.I objfil\^
or
.I corfil\^.
On the 3B-20,
an initial argument of
.B  \-w
permits overwriting locations in
.I objfil\^.
.SS Addresses.
The address in a file associated with
a written address is determined by a mapping
associated with that file.
Each mapping is represented by two triples
.RI ( "b1, e1, f1" )
and
.RI ( "b2, e2, f2" )
and the
.I file address\^
corresponding to a written
.I address\^
is calculated as follows:
.PP
.RS
.IR b1 \*(LE address < e1
\*(IM
.IR "file address" = address + f1\-b1
.RE
otherwise
.PP
.RS
.IR b2 \*(LE address < e2
\*(IM
.IR "file address" = address + f2\-b2,
.RE
.PP
otherwise, the requested
.I address\^
is not legal.
In some cases (e.g. for programs with separated I and D
space) the two segments for a file may overlap.
If a
.B ?
or
.B /
is followed by an
.B \(**
then only the second triple is used.
.PP
The initial setting of both mappings is suitable for
normal
.B a.out 
and
.B core 
files.
If either file is not of the kind expected then, for that file,
.I b1\^
is set to 0,
.I e1\^
is set to
the maximum file size,
and
.I f1\^
is set to 0; in this way the whole
file can be examined with no address translation.
.PP
In order for
.I sdb\^
to be used on large files
all appropriate values are kept as signed 32 bit integers.
.SS Commands.
.PP
The commands for examining data in the program are:
.TP 5
.B t
Print a stack trace of the terminated or stopped program.
.TP 5
.B T
Print the top line of the stack trace.
.TP 5
.IB variable / lm
Print the value of
.I variable\^
according to
length
.I l\^
and format 
.IR m .
If
.I l\^
and
.I m\^
are omitted,
.I sdb\^
chooses a length and format
suitable for the variable's type
as declared in the program.
The length specifiers are:
.RS
.RS
.PD 0
.TP
.BI b\^
one byte
.TP
.BI h\^
two bytes (half word)
.TP
.BI l\^
four bytes (long word)
.RE
.PD
.br
.ne 5
.PP
Legal values for
.I m\^
are:
.RS
.PD 0
.TP
.BI c\^
character
.TP
.BI d\^
decimal
.TP
.BI u\^
decimal, unsigned
.TP
.BI o\^
octal
.TP
.BI x\^
hexadecimal
.TP
.BI f\^
32 bit single precision floating point
.TP
.BI g\^
64 bit double precision floating point
.TP
.BI s\^
Assume
.I variable\^
is a string pointer and print characters starting at the
address pointed to by the variable.
.TP
.BI a\^
Print characters starting at the variable's address.
.TP
.BI p\^
pointer to procedure
.TP
.BI i\^
disassemble machine language instruction
with addresses printed symbolically.
.TP
.BI I\^
disassemble machine language instruction
with addresses just printed numerically.
.RE
.PD
The length specifiers are only effective with the formats
\fBd\fP, \fBu\fP, \fBo\fP and \fBx\fP.
If one of these formats is specified and
.I l\^
is omitted, the length defaults to
the word length of the host machine:
4 for the 3B-20
and \s-1VAX\s+11/780.
If a numeric length specifier is used for the
.B s
or
.B a
command then that many characters are printed.
Otherwise successive characters are printed until either
a null byte is reached
or 128 characters are printed.
The last variable may be redisplayed with the command
.BR ./ .
.PP
The
.IR sh (1)
metacharacters 
.B \(**
and
.B ?
may be used within procedure and variable names,
providing a limited form of pattern matching.
If no procedure name is given, both variables local to the current
procedure and global (common for F77) variables are matched,
while if a procedure name is specified then
only variables local to that procedure are matched.
To match only global variables (or blank common for F77),
the form
.BI : pattern\^
is used.
The name of a common block may be specified
instead of a procedure name for F77 programs.
.RE
.TP 5
.PD 0
.IB linenumber ? lm
.TP 5
\fIvariable:\fB?\fIlm\fR (3B-20 only)
.PD
Print the value at the address
from
.BR a.out
or I space
given by
.I linenumber\^
or
.IR variable
(3B-20 only),
according to the format
.IR lm .
The default format is `i'.
.TP 5
.PD 0
.IB variable = lm
.TP 5
.IB linenumber = lm
.TP 5
.IB number = lm
.PD
Print the address of
.I variable\^
or
.IR linenumber ,
or the value of
.IR number ,
in the format specified by
.IR lm .
If no format is given, then
.B lx
is used.
The last variant of this command provides a convenient way to convert
between decimal, octal and hexadecimal.
.TP 5
.IB variable ! value
Set
.I variable\^
to the given
.IR value .
The value may be a number, character constant or a variable.
If the variable is of type float or double,
the value may also be a floating constant.
.TP 5
.B x
Print the machine registers and
the current machine language instruction.
.TP 5
.B X
Print the current machine language instruction.
.PP
The commands for examining source files are:
.PP
.PD 0
.TP 5
.BI "e " procedure\^
.TP 5
.BI "e " file-name\^
.PD
Set the current file to
the file containing
.I procedure\^
or to
.IR file-name .
Set the current line to the first line in the named
procedure or file.
All source files are assumed to be in
.IR directory .
The default is the current working directory.
If no procedure or file name is given,
the current procedure name and file names
are reported.
.TP 5
.BI / "regular expression" /
Search forward from the current line for a line containing
a string matching 
.I regular expression\^
as in
.IR ed (1).
The trailing
.B /
may be elided.
.TP 5
.BI ? "regular expression" ?
Search backward from the current line for a line containing
a string matching
.I regular expression\^
as in
.IR ed (1).
The trailing
.B ?
may be elided.
.TP 5
.B p
Print the current line.
.TP 5
.B z
Print the current line followed by the next 9 lines.
Set the current line to the last line printed.
.TP 5
.B w
Window.
Print the 10 lines around the current line.
.TP 5
.I number\^
Set the current line to the given line number.
Print the new current line.
.TP 5
.IB count +
Advance the current line by
.I count\^
lines.
Print the new current line.
.TP 5
.IB count \(mi
Retreat the current line by
.I count\^
lines.
Print the new current line.
.PP
The commands for controlling the execution of the source program are:
.PP
.TP 5
\fIcount\fB r \fIargs\fR
.br
.ns
.TP 5
\fIcount\fB R
Run the program with the given arguments.
The \fBr\fP command with no arguments reuses the previous arguments
to the program while the \fBR\fP command
runs the program with no arguments.
An argument beginning with
.B <
or
.B >
causes redirection for the
standard input or output respectively.
If \fIcount\fP is given,
it specifies the number of breakpoints to be ignored.
.TP 5
\fIlinenumber\fB c\fI count\fR
.br
.ns
.TP 5
\fIlinenumber\fB C\fI count\fR
Continue after a breakpoint or interrupt.
If \fIcount\fP is given,
it specifies the number of breakpoints to be ignored.
\fBC\fP continues with the signal which caused the program to stop
and
\fBc\fP ignores it.
If a linenumber is specified
then a temporary breakpoint is placed at the line
and execution is continued.
The breakpoint is deleted when the command finishes.
.TP 5
\fIlinenumber\fB g\fI count\fR
Continue after a breakpoint
with execution resumed at the given line.
If \fIcount\fP is given,
it specifies the number of breakpoints to be ignored.
.TP 5
\fBs \fIcount\fR
.br
.ns
.TP 5
\fBS \fIcount\fR
Single step.
Run the program through \fIcount\fP lines.
If no count is given then the program is run for one line.
.B S
is equivalent to
.B s
except it steps through procedure calls.
.TP 5
\fBi\fR
.br
.ns
.TP 5
\fBI\fR
Single step by one machine language instruction.
\fBI\fP steps with the signal
which caused the program to stop and
\fBi\fP ignores it.
.TP 5
\fIvariable$\fBm \fIcount\fR
.br
.ns
.TP 5
\fIaddress:\fBm \fIcount\fR (3B-20 only)
Single step
(as with \fBs\fP)
until the specified location
is modified with a new value.
If \fIcount\fP is omitted,
it is effectively infinity.
\fIVariable\fR
must be accessible from the current procedure.
.TP 0
\fIToggle verbose mode\fR,
for use when single stepping with
\fBS\fP,
\fBs\fP or
\fBm\fP.
If \fIlevel\fP is omitted,
then just the current
source file and/or subroutine name
is printed when either changes.
If \fIlevel\fP is 1 or greater,
each C source line is printed
before it is executed;
if \fIlevel\fP is 2 or greater,
each assembler statement
is also printed.
A \fBv\fP turns verbose mode off if it is on for any level.
.TP 5
.B k
Kill the debugged program.
.TP 5
procedure\fB(\fParg1,arg2,...\fB)\fP
.br
.ns
.TP 5
procedure\fB(\fParg1,arg2,...\fB)/\fP\fIm\fP
Execute the named procedure with the given arguments.
Arguments can be integer, character or string constants
or names of variables accessible from the current procedure.
The second form causes the value returned by the procedure to be
printed according to format \fIm\fP.
If no format is given, it defaults to
.BR d .
.TP 5
\fIlinenumber\fB b\fR \fIcommands\fR
Set a breakpoint at the given line.
If a procedure name without a line number is given (e.g. ``proc:''),
a breakpoint is placed at the first line in the procedure
even if it was not compiled with the
debug
flag.
If no \fIlinenumber\fP is given,
a breakpoint is placed at the current line.
If no
.I commands\^
are given then execution stops just before the breakpoint
and control is returned to
.IR sdb .
Otherwise
the 
.I commands\^
are executed when the breakpoint is
encountered and execution continues.
Multiple commands are specified by separating them with semicolons.
If \fBk\fP
is used as a command to execute at a breakpoint,
control returns to
.IR sdb ,
instead of continuing execution.
.TP 5
.B B
Print a list of the currently active breakpoints.
.TP 5
\fIlinenumber\fB d\fR
Delete a breakpoint at the given line.
If no \fIlinenumber\fP is given then the breakpoints are deleted interactively:
Each breakpoint location is printed and a line is read from the standard input.
If the line begins with a
.B y
or
.B d
then the breakpoint is deleted.
.TP 5
.B D
Delete all breakpoints.
.TP 5
.B l
Print the last executed line.
.TP 5
\fIlinenumber\fB a\fR
Announce.
If \fIlinenumber\fR is of the form
.IB proc : number\fR,
the command
effectively does a
.IB "linenumber " "b l\fR.
If \fIlinenumber\fR is of the form
.IB proc :\fR,
the command
effectively does a
.IB proc ": b T"\fR.
.PP
Miscellaneous commands:
.TP 5
.BI ! command\^
The command is interpreted by
.IR sh (1).
.TP 5
.B new-line
If the previous command printed a source line then
advance the current line by 1 line and
print the new current line.
If the previous command displayed a core location then
display the next core location.
.TP 5
.B control-D
Scroll.
Print the next 10 lines
of instructions, source or data
depending on which was printed last.
.TP 5
.B M
Print the address maps.
.TP 5
.BR M\ [ ?/ ][ \(** "] \fIb \|e \|f\fP"
New values for the address map are recorded.
The arguments
\fB?\fP and \fB/\fP
specify the text and data maps respectively.
The first segment,
.RI ( "b1, e1, f1" )
is changed unless \fB\(**\fP is specified,
in which case the second segment,
.RI ( "b2, e2, f2" )
of the mapping is changed.
If fewer than three values are given,
the remaining map parameters are left unchanged.
.TP 5
\fB"\fI string\fR
Print the given string.
.TP 5
.B q
Exit the debugger.
.PP
The following commands also exist and are intended only for
debugging the debugger:
.PP
.PD 0
.TP 5
.B V
Print the version number.
.TP 5
.B Q
Print a list of procedures and files being debugged.
.TP 5
.B Y
Toggle debug output.
.PD
.SH FILES
a.out
.br
core
.SH SEE ALSO
a.out(4), core(4).
.SH BUGS
If a procedure is called when the program is
.I not\^
stopped at a breakpoint
(such as when a core image is being debugged),
all variables are initialized before the procedure is started.
This makes it impossible to use a procedure which formats
data from a core image.
.PP
Arrays must be of one dimension
and of zero origin
to be correctly addressed by
.IR sdb.\^
.PP
The default type for printing F77 parameters is incorrect.
Their address is printed instead of their value.
.PP
Tracebacks containing F77 subprograms with multiple entry points
may print too many arguments in the wrong order, but their values
are correct.
.PP
On the
3B-20
there is no hardware trace mode
and single stepping is implemented
by setting pseudo breakpoints where possible.
