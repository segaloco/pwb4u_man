.TH LP 1
.SH NAME
lp, cancel \- send/cancel requests to an \s-1LP\s+1 line printer
.SH SYNOPSIS
.B lp
.RB [ \-c ]
.RB [ \-d \^dest]
.RB [ \-m ]
.RB [ \-n \^number]
.RB [ \-o \^option]
.RB [ \-t \^title]
.RB [ \-w ]
files
.br
.B cancel
[\|ids\|] [\|printers\|]
.SH DESCRIPTION
.I Lp
arranges
for the named files
and associated information
(collectively called a
.IR request )
to be printed
by a line printer.
If no file names
are mentioned,
the standard input
is assumed.
The file name
.B \-
stands for
the standard input
and may be supplied
on the command line
in conjunction
with named
.IR files .
The order
in which
.I files
appear
is the same order
in which they will be printed.
.PP
.I Lp
associates
a unique
.I id
with each request
and prints it
on the standard output.
This
.I id
can be used later
to cancel
(see
.IR cancel )
or find the status
(see
.IR lpstat (1))
of the request.
.PP
The following options
to
.I lp
may appear
in any order
and may be intermixed
with file names:
.TP "\w'\-n\|number\ \ 'u"
.B \-c
Make copies
of the
.I files
to be printed
immediately when
.I lp
is invoked.
Normally,
.I files
will not be copied,
but will be linked
whenever possible.
If the
.B \-c
option is not given,
then the user
should be careful
not to remove
any of the
.I files
before the request
has been printed
in its entirety.
It should also be noted
that in the absence
of the
.B \-c
option,
any changes made
to the named
.I files
after the request
is made
but before it is printed
will be reflected
in the printed output.
.TP
.BI \-d dest
Choose
.I dest
as the printer
or class of printers
that is
to do the printing.
If
.I dest
is a printer,
then the request
will be printed only
on that specific printer.
If
.I dest
is a class of printers,
then the request
will be printed
on the first available printer
that is a member
of the class.
Under certain conditions
(printer unavailability,
file space limitation,
etc.),
requests for
specific destinations
may not be accepted
(see
.IR lpstat (1)).
By default,
.I dest
is taken from
the environment variable
.SM
.B LPDEST
(if it is set).
Otherwise,
a default destination
(if one exists)
for the computer system
is used.
Destination names vary
between systems
(see
.IR lpstat (1)).
.TP
.B \-m
Send mail
(see
.IR mail(1))
after the files
have been printed.
By default,
no mail is sent
upon normal completion
of the print request.
.TP
.BI \-n number
Print
.I number
copies (default of 1) of the output.
.TP
.BI \-o option
Specify printer-dependent or class-dependent
.IR options .
The
.B \-o
keyletter may be specified more than once.
See
.I Models
below for a list of currently supported
models and the options that they may be given.
.TP
.BI \-t title
.PP
Print
.I title
on the banner page
of the output.
.TP
.B \-w
.PP
Write a message
on the user's
terminal after the
.I files
have been printed.
If the user
is not logged in,
then mail will be sent instead.
.PP
.I Cancel
cancels line printer requests
that were made by the
.IR lp
command.
The command line arguments
may be either request
.I ids
(as returned by
.IR lp)
or
.I printer
names
(for a complete list,
use
.IR lpstat (1)).
Specifying a request
.I id
cancels the associated request
even if it is currently printing.
Specifying a
.I printer
cancels the request
which is currently printing
on that printer.
In either case,
the cancellation
of a request
that is currently printing
frees the printer
to print its next available request.
.SS Models.
Model printer interface programs are supplied
with the
.SM LP
software.
They are shell procedures which interface
between
.I lpsched
and devices.
The following list
describes the
.I models
and
lists the options which they may be given on the
.I lp
command line
using the
.B \-o
keyletter:
.TP "\w'dumb\ \ \ \ 'u"
.B dumb
interface for a line printer without special functions and protocol.
Form feeds are assumed.
This is a good model to copy and modify for printers
which do not have models.
.TP
.B 1640
Diablo 1640 terminal
running at 1200 baud,
using
.SM XON\s+1/\s-1XOFF
protocol.
Options:
.RS
.TP
.PD 0
.B \-12
12-pitch
(10-pitch is the default)
.TP
.B \-f
don't use the
.IR 450 (1)
filter.
The output has been pre-processed
by either
.IR 450 (1)
or the
\fInroff\fP 450 driving table.
.RE
.PD
.TP
.B hp
Hewlett Packard 2631A line printer at 2400 baud.
Options:
.RS
.TP
.PD 0
.B \-c
compressed print
.TP
.B \-e
expanded print
.RE
.PD
.TP
.B prx
Printronix P300 printer using
.SM XON\s+1/\s-1XOFF
protocol at 4800 baud using a serial interface.
.TP
.B pprx
Printronix printer using a parallel interface.
.TP
.B 127
Sanders Media 12/7 printer with
.SM XON\s+1/\s-1XOFF
protocol at 2400 baud using a serial interface.
.SH EXAMPLES
.TP 4
1.
Assuming there is an existing Hewlett Packard 2631A line printer named
.IR hp2
that is using the model
.B hp
interface,
compressed print
may be obtained
using the command:
.PP
.RS 10
lp\ \ \-dhp2\ \ \-o\-c\ \ files
.RE
.TP 4
2.
An \fInroff\fP document may be printed on
a Diablo 1640 printer called \fIst1\fP,
using the model
.B 1640
interface,
in any of the following ways:
.PP
.RS 10
nroff\ \ \-T450\ \ files\ \ \(bv\ \ lp\ \ \-dst1\ \ \-of
.br
nroff\ \ \-T450\-12\ \ files\ \ \(bv\ \ lp\ \ \-dst1\ \ \-of
.br
nroff\ \ \-T37\ \ files\ \ \(bv\ \ col\ \ \(bv\ \ lp\ \ \-dst1
.RE
.TP 4
3.
The following command prints the password file on
.I st1
in 12-pitch:
.PP
.RS 10
lp\ \ \-dst1\ \ \-o12\ \ /etc/passwd
.RE
.TP 4
\&
.SM
.I NOTE:
the
.B \-12
option
to the
.B 1640
model
should never be used in conjunction
with
.IR nroff .
.SH FILES
/usr/spool/lp/\(**
.SH SEE ALSO
enable(1),
lpstat(1),
mail(1).
