.TH X25PVC 1
.SH NAME
x25pvc, x25lnk \- install, remove, or get status
for a \s-1PVC\s+1 or \s-1BX.25\s+1 link
.SH SYNOPSIS
.B x25pvc
options
.br
.B x25lnk
options
.SH DESCRIPTION
.I X25pvc\^
may be used to install or remove a
.SM BX.25
Permanent Virtual Circuit
.SM (PVC)
on a specified
.SM BX.25
interface (
.I link
), or to display the status of
a specified
.SM BX.25
minor device (
.I slot
).
Exactly one of the following options
must be used:
.TP
.BI \-i " slotname chno linkno"
.I Slotname\^
is a path name that specifies a
.SM BX.25
minor device (slot).
If the minor device is currently
connected to some logical channel
on some
.SM BX.25
interface (link), then first
that minor device will be
removed, if possible (see the
.B \-r
option).
If that minor device is available,
it is connected to logical channel
.I chno
on link number
.IR linkno .
.I Chno\^
must be in the range of 1 to 4,095
and must not be currently in use
for any other
.SM BX.25
minor device associated with that link.
.TP
.BI \-r " slotname"
Remove
.SM BX.25
minor device
.IR slotname .
The command will fail if the slot is open,
if packets are waiting to be transmitted,
or if there are unacknowledged packets outstanding.
.TP
.BI \-s " slotname"
Print the status of
.SM BX.25
minor device
.IR slotname .
The information printed
consists of
.IR slotname ,
the logical channel number,
the link number.
.PP
.I X25lnk
is used to activate or deactivate
a specified
.SM BX.25
link.
Exactly one of the following
options must be used:
.TP
.BI "\-i [\-b] [\-f]" " linkno " "[ 82 ]"
Activate the
.SM BX.25
link that is specified by
.IR linkno .
The
.B \-b
option specifies that the link-level
protocol will use Address B.
The default is Address A.
The
.B \-f
option specifies to the hardware
that the high speed V.35 interface
should be used.
.I Linkno\^
is the number of the
.SM BX.25
link to be installed.
The
.B 82
option specifies that the link
to be started is on a
.SM TN82.
The default is that the link is on a
.SM TN75.
The
.B \-i
option makes the necessary connections
between data structures and
starts the
.SM BX.25
protocol on the link.
.TP
.BI \-h " linkno [ " 82 " ]"
Halt the link specified by
.IR linkno .
The
.B 82
option specifies that the linkno
is on a
.SM TN82.
.TP
.BI \-s " linkno [ " 82 " ]"
Print the status of the link specified by
.IR linkno .
The information printed consists
of the link number,
the packet size used on that link.
The
.B 82
option specifies that the linkno is on a
.SM TN82.
.SH SEE ALSO
vpmc(1C).
.br
.I Operations Systems Network Protocol Specification: BX.25 Issue 2
, Bell Laboratories.