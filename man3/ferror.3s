.TH FERROR 3S
.SH NAME
ferror, feof, clearerr, fileno \- stream status inquiries
.SH SYNOPSIS
.B #include <stdio.h>
.PP
.B int feof (stream)
.br
.SM
.B FILE
.B \(**stream;
.PP
.B int ferror (stream)
.br
.SM
.B FILE
.B \(**stream
.PP
.B clearerr (stream)
.br
.SM
.B FILE
.B \(**stream
.PP
.B fileno(stream)
.br
.SM
.B FILE
.B \(**stream;
.SH DESCRIPTION
.I Feof\^
returns non-zero when end of file is read on the named input
.IR stream ,
otherwise zero.
.PP
.I Ferror\^
returns non-zero when error has occurred reading or writing
the named
.IR stream ,
otherwise zero.
Unless cleared by
.IR clearerr ,
the error indication lasts until
the stream is closed.
.PP
.I Clearerr\^
resets the error indication on the named
.IR stream .
.PP
.I Fileno\^
returns the integer file descriptor
associated with the
.IR stream ,
see
.IR open (2).
.PP
.IR Feof ,
.IR ferror ,
and
.I fileno\^
are implemented as macros;
they cannot be re-declared.
.SH SEE ALSO
open(2),
fopen(3S).
