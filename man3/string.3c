.TH STRING 3C 
.SH NAME
strcat, strncat, strcmp, strncmp, strcpy, strncpy, strlen, strchr, strrchr, strpbrk, strspn, strcspn, strtok \- string operations
.SH SYNOPSIS
.B char \(**strcat (s1, s2)
.br
.B char \(**s1, \(**s2;
.PP
.B char \(**strncat (s1, s2, n)
.br
.B char \(**s1, \(**s2;
.br
.B int n;
.PP
.B int strcmp (s1, s2)
.br
.B char \(**s1, \(**s2;
.PP
.B int strncmp (s1, s2, n)
.br
.B char \(**s1, \(**s2;
.br
.B int n;
.PP
.B char \(**strcpy (s1, s2)
.br
.B char \(**s1, \(**s2;
.PP
.B char \(**strncpy (s1, s2, n)
.br
.B char \(**s1, \(**s2;
.br
.B int n;
.PP
.B int strlen (s)
.br
.B char \(**s;
.PP
.B char \(**strchr (s, c)
.br
.B char \(**s, c;
.PP
.B char \(**strrchr (s, c)
.br
.B char \(**s, c;
.PP
.B char \(**strpbrk (s1, s2)
.br
.B char \(**s1, \(**s2;
.PP
.B int strspn (s1, s2)
.br
.B char \(**s1, \(**s2;
.PP
.B int strcspn (s1, s2)
.br
.B char \(**s1, \(**s2;
.PP
.B char \(**strtok (s1, s2)
.br
.B char \(**s1, \(**s2;
.SH DESCRIPTION
These functions operate on null-terminated strings.
They do not check for overflow of any receiving string.
.PP
.I Strcat\^
appends a copy of string
.I s2\^
to the end of string
.IR s1 .
.I Strncat\^
copies at most
.I n\^
characters.
Both return a pointer to the null-terminated result.
.PP
.I Strcmp\^
compares its arguments and returns an integer
greater than, equal to, or less than 0,
according as
.I s1\^
is lexicographically greater than, equal to, or
less than
.IR s2 .
.I Strncmp\^
makes the same comparison but looks at at most
.I n\^
characters.
.PP
.I Strcpy\^
copies string
.I s2\^
to
.IR s1 ,
stopping after the null character has been moved.
.I Strncpy\^
copies exactly
.I n\^
characters,
truncating or null-padding
.IR s2 ;
the target may not be null-terminated if the length
of
.I s2\^
is
.I n\^
or more.
Both return
.IR s1 .
.PP
.I Strlen\^
returns the number of non-null characters in
.IR s .
.PP
.I Strchr\^
.RI ( strrchr )
returns a pointer to the first (last)
occurrence of character 
.I c\^
in string
.IR s ,
or
.SM
.B
NULL
if
.I c\^
does not occur in the string.
The null character terminating a string is considered to
be part of the string.
.PP
.I Strpbrk\^
returns a pointer to the first occurrence in string
.I s1\^
of any character from string
.IR s2 ,
or
.SM
.B
NULL
if no character from
.I s2\^
exists in
.IR s1 .
.PP
.I Strspn\^
.RI ( strcspn )
returns the length of the initial segment of string
.I s1\^
which consists entirely of characters from (not from) string
.IR s2 .
.PP
.I Strtok\^
considers the string
.I s1\^
to consist of a sequence of zero or more text tokens separated
by spans of one or more characters from the separator string
.IR s2 .
The first call (with pointer
.I s1\^
specified) returns a pointer to the first character of the first
token, and will have written a
.SM
.B
NULL
character into
.I s1\^
immediately following the returned token.  Subsequent calls
with zero for the first argument, will work through the string
.I s1\^
in this way until no tokens remain.
The separator string
.I s2\^
may be different from call to call.
When no token remains in
.IR s1 ,
a
.SM
.B
NULL
is returned.
.SH BUGS
.I Strcmp\^
uses native character comparison, which is signed
on
.SM PDP\*S-11s,
unsigned on other machines.
.PP
All string movement is performed character by character starting
at the left.
Thus overlapping moves toward the left will work as expected,
but overlapping moves to the right may yield surprises.
