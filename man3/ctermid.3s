.TH CTERMID 3S
.SH NAME
ctermid \- generate file name for terminal
.SH SYNOPSIS
.B #include <stdio.h>
.PP
.B char \(**ctermid(s)
.br
.B char \(**s;
.SH DESCRIPTION
.I Ctermid\^
generates a string that refers to the controlling
terminal for the current process when used as a file name.
.PP
If
.RI (int) s\^
is zero, the string is stored in an internal static area,
the contents of which are overwritten at the next call to
.IR ctermid ,
and the address of which is returned.
If
.RI (int) s\^
is non-zero, then
.I s\^
is assumed to point to a character array of at least
.B L_ctermid
elements; the string is placed in this array and the value of
.I s\^
is returned.
The manifest constant
.B L_ctermid
is defined in
.BR <stdio.h> .
.SH NOTES
The difference between
.I ctermid\^
and
.IR ttyname (3C)
is that
.I ttyname\^
must be handed a file descriptor and returns the actual name of
the terminal associated with that file descriptor, while
.I ctermid\^
returns a magic string
.RB ( /dev/tty )
that will refer to the
terminal if used as a file name.
Thus
.I ttyname\^
is useless unless the process already has at least one file open
to a terminal.
.SH SEE ALSO
ttyname(3C).
