'\" e
.if n .ll 79
.TH DRAND48 3C 
.EQ
delim $$
.EN
.SH NAME
drand48, lrand48, mrand48, srand48, seed48, lcong48 \- uniform random number generators
.SH SYNOPSIS
.nf
.B double drand48 ( )
.PP
.B long lrand48 ( )
.PP
.B long mrand48 ( )
.PP
.B srand48 (seedval)
.B long seedval;
.PP
.B seed48 (seed16v)
.B short seed16v[3];
.PP
.B lcong48 (param)
.B short param[7];
.SH DESCRIPTION
This is a family of interfaces to a good pseudo-random number generator
based upon the linear congruential algorithm and 48 bit integer arithmetic.
.PP
.I Drand48\^
returns the next pseudo-random number as a
non-negative valued double-precision binary fraction.
The values returned are
uniformly distributed over the interval $[0,~1).$
.PP
.I Lrand48\^
returns the next pseudo-random number as a
non-negative valued long integer.
The values returned are
uniformly distributed over the
interval $[0,~2\(**\(**31 ).$
.PP
.I Mrand48\^
returns the next pseudo-random number as a 
signed long integer.
The values returned are
uniformly distributed over the interval
$[-2\(**\(**31 ,~2\(**\(**31 ).$
.PP
.I Srand48\^
initializes the generator according to the argument
.I seedval.
.PP
.I Seed48\^
initializes the generator according to the 48-bit integer
represented in the array
.I seedv16.
Suitable defaults are supplied for the two initializing
functions, or if no initialization is used.
.PP
.I Lcong48\^
permits the parameters of the linear congruential generator
to be specified.
The array of seven short integers passed to
.I lcong48\^
is interpreted as two 48 bit integers
(the initial value, and the multiplier)
and a short integer
(the addend)
used to construct the linear congruential algorithm.
.SH SEE ALSO
rand(3C).
