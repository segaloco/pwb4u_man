.TH UNGETC 3S 
.SH NAME
ungetc \- push character back into input stream
.SH SYNOPSIS
.B #include <stdio.h>
.PP
.B int ungetc (c, stream)
.br
.B char c;
.br
.SM
.B FILE
.B \(**stream;
.SH DESCRIPTION
.I Ungetc\^
pushes the character
.I c\^
back on an input stream.
That character will be returned by the next
.I getc\^
call on that stream.
.I Ungetc\^
returns 
.IR c .
.PP
One character of pushback is guaranteed provided
something has been read from the stream and the stream is
actually buffered.
Attempts to push
.B EOF
are rejected.
.PP
.IR Fseek (3S)
erases all memory of pushed back characters.
.SH "SEE ALSO"
fseek(3S), getc(3S), setbuf(3S).
.SH DIAGNOSTICS
.I Ungetc\^
returns
.SM
.B EOF
if it can't push a character back.
