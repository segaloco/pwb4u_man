'\" t
.TH FS 4
.SH NAME
file system \- format of system volume
.SH SYNOPSIS
.B #include <sys/filsys.h>
.br
.B #include <sys/types.h>
.br
.B #include <sys/param.h>
.SH DESCRIPTION
Every
file system storage volume
(e.g., \s-1RP\s+1\&04 disk)
has a common format for certain vital information.
Every such volume is divided into a certain number
of 256 word (512 byte) blocks.
Block 0 is unused and is available to contain
a bootstrap program or other information.
.PP
Block 1 is the
.IR super-block .
Starting from its first word, the format of a super-block is:
.PP
.nf
/\(**	@(#)filsys.h					1.1 \(**/
/\(**
 \(** Structure of the super-block
 \(**/
.TS
l1 l1 l1 l.
struct	filsys
{
	ushort	s_isize;	/\(** size in blocks of i-list \(**/
	daddr_t	s_fsize;	/\(** size in blocks of entire volume \(**/
	short	s_nfree;	/\(** number of addresses in s_free \(**/
	daddr_t	s_free[\s-1NICFREE\s+1];	/\(** free block list \(**/
	short	s_ninode;	/\(** number of i-nodes in s_inode \(**/
	ino_t	s_inode[\s-1NICINOD\s+1];	/\(** free i-node list \(**/
	char	s_flock;	/\(** lock during free list manipulation \(**/
	char	s_ilock;	/\(** lock during i-list manipulation \(**/
	char  	s_fmod; 	/\(** super block modified flag \(**/
	char	s_ronly;	/\(** mounted read-only flag \(**/
	time_t	s_time; 	/\(** last super block update \(**/
	short	s_dinfo[4];	/\(** device information \(**/
	daddr_t	s_tfree;	/\(** total free blocks\(**/
	ino_t	s_tinode;	/\(** total free inodes \(**/
	char	s_fname[6];	/\(** file system name \(**/
	char	s_fpack[6];	/\(** file system pack name \(**/
};
.PP
.I S_isize\^
is the address of the first data block after the i-list;
the i-list starts just after the super-block, namely in block 2;
thus the i-list is \f2s_isize\^\fP\-2 blocks long.
.I S_fsize\^
is the first block not potentially available for allocation
to a file.
These numbers are used by the system to
check for bad block numbers;
if an ``impossible'' block number is allocated from the free list
or is freed,
a diagnostic is written on the on-line console.
Moreover, the free array is cleared, so as to prevent further
allocation from a presumably corrupted free list.
.PP
The free list for each volume is maintained as
follows.
The
.I s_free\^
array contains, in
.IR s_free [1],
\&.\|.\|.,
.IR s_free [ s_nfree \-1],
up to 49 numbers of free blocks.
.IR S_free [0]
is the block number of the head
of a chain of blocks constituting the free list.
The first long in each free-chain block is the number
(up to 50) of free-block numbers listed in the
next 50 longs of this chain member.
The first of these 50 blocks is the link to the
next member of the chain.
To allocate a block:
decrement
.IR s_nfree ,
and the new block is
.IR s_free [ s_nfree ].
If the new block number is 0,
there are no blocks left, so give an error.
If
.I s_nfree\^
became 0,
read in the block named by the new block number,
replace
.I s_nfree\^
by its first word,
and copy the block numbers in the next 50 longs into the
.I s_free\^
array.
To free a block, check if
.I s_nfree\^
is 50; if so,
copy
.I s_nfree\^
and the
.I s_free\^
array into it,
write it out, and set
.I s_nfree\^
to 0.
In any event set
.IR s_free [ s_nfree ]
to the freed block's number and
increment
.IR s_nfree .
.PP
.I S_tfree\^
is the total free blocks available in the file system.
.PP
.I S_ninode\^
is the number of free i-numbers in the
.I s_inode\^
array.
To allocate an i-node:
if
.I s_ninode\^
is greater than 0,
decrement it and return
.IR s_inode [ s_ninode ].
If it was 0, read the i-list
and place the numbers of all free inodes
(up to 100) into the
.I s_inode\^
array,
then try again.
To free an i-node,
provided
.I s_ninode\^
is less than 100,
place its number into
.IR s_inode [ s_ninode ]
and increment
.IR s_ninode .
If
.I s_ninode\^
is already 100, do not bother to enter the freed i-node into any table.
This list of i-nodes is only to speed
up the allocation process; the information
as to whether the inode is really free
or not is maintained in the inode itself.
.PP
.I S_tinode\^
is the total free inodes available in the file system.
.PP
.I S_flock\^
and
.I s_ilock\^
are flags maintained in the core
copy of the file system
while it is mounted
and their values on disk are immaterial.
The value of
.I s_fmod\^
on disk is likewise immaterial;
it is used as a flag to indicate that the super-block has
changed and should be copied to
the disk during the next periodic update of file
system information.
.PP
.I S_ronly\^
is a read-only flag to indicate write-protection.
.PP
.I S_time\^
is the last time the super-block of the file system was changed,
and is a double-precision representation
of the number of seconds that have elapsed
since
00:00 Jan. 1, 1970 (\s-1GMT\s+1).
During a reboot, the
.I s_time\^
of the super-block for the root file system
is used to set the system's idea of the time.
.PP
.I S_fname\^
is the name of the file system and
.I s_fpack\^
is the name of the pack.
.PP
I-numbers begin at 1, and the storage for i-nodes
begins in block 2.
Also, i-nodes are 64 bytes long, so 8 of them fit into a block.
Therefore, i-node
.I i\^
is located in block (\fIi\^\fR+15)/8, and begins
64\(mu((\fIi\^\fR+15)\ (mod 8))
bytes from its start.
I-node 1 is reserved for future use.
I-node 2 is reserved for the root directory of the file
system, but no other i-number has a built-in
meaning.
Each i-node represents one file.
For the format of an inode and its flags, see
.IR inode (4).
.SH FILES
/usr/include/sys/filsys.h
.br
/usr/include/sys/stat.h
.SH SEE ALSO
fsck(1M), fsdb(1M), mkfs(1M), inode(4).
